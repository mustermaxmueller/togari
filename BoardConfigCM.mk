BOARD_CUSTOM_BOOTIMG := true
BOARD_KERNEL_SEPARATED_DT := true
TARGET_DTB_EXTRA_FLAGS := --force-v2
BOARD_CUSTOM_BOOTIMG_MK := device/sony/rhine/boot/custombootimg.mk

TARGET_USERIMAGES_USE_F2FS := true
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := f2fs

# use CAF variants
BOARD_USES_QCOM_HARDWARE := true

# Power HAL
TARGET_POWERHAL_VARIANT := qcom
CM_POWERHAL_EXTENSION := qcom

#TARGET_USE_ION_COMPAT := true

# Time
#BOARD_USES_QC_TIME_SERVICES := true

#TARGET_USES_C2D_COMPOSITION := true
#KRAIT_BIONIC_OPTIMIZATION := true

TARGET_HW_DISK_ENCRYPTION := true
TARGET_ENABLE_QC_AV_ENHANCEMENTS := true

# Snapdragon Camera
PRODUCT_PACKAGES += \
    SnapdragonCamera

PRODUCT_PROPERTY_OVERRIDES += \
    keyguard.no_require_sim=false
    #qemu.hw.mainkeys=1

# Recovery
BOARD_HAS_NO_SELECT_BUTTON := true

# TWRP flags
TARGET_RECOVERY_PIXEL_FORMAT := "RGB_565"
TW_THEME := portrait_hdpi
RECOVERY_GRAPHICS_USE_LINELENGTH := true
TW_HAS_NO_RECOVERY_PARTITION := true
TW_FLASH_FROM_STORAGE := true
TW_EXTERNAL_STORAGE_PATH := "/external_sd"
TW_EXTERNAL_STORAGE_MOUNT_POINT := "external_sd"
TW_DEFAULT_EXTERNAL_STORAGE := true
TW_INCLUDE_JB_CRYPTO := false
TW_INCLUDE_L_CRYPTO := true
TW_BRIGHTNESS_PATH := /sys/class/leds/lcd-backlight/brightness
TW_MAX_BRIGHTNESS := 4095
TW_NO_USB_STORAGE := true
TW_CRYPTO_FS_TYPE := "f2fs"
TW_CRYPTO_REAL_BLKDEV := "/dev/block/platform/msm_sdcc.1/by-name/userdata"
TW_CRYPTO_MNT_POINT := "/data"
TW_CRYPTO_FS_OPTIONS := "noatime,nosuid,nodev,discard,inline_xattr"
TW_CRYPTO_FS_FLAGS := "0x00000406"
TW_CRYPTO_KEY_LOC := "footer"
TW_INCLUDE_FUSE_EXFAT := true

# MultiROM config. MultiROM also uses parts of TWRP config
# MR_INPUT_TYPE := type_b
# MR_INIT_DEVICES := device/sony/rhine/multirom/init_devices.c
# MR_DPI := xhdpi
# MR_KEXEC_DTB := true
# MR_DPI_FONT := 340
# MR_FSTAB := device/sony/rhine/multirom/twrp.fstab
# MR_USE_MROM_FSTAB := true
# MR_KEXEC_MEM_MIN := 0x20000000
