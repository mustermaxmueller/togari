TARGET_SCREEN_HEIGHT := 1920
TARGET_SCREEN_WIDTH := 1080
 
# Set those variables here to overwrite the inherited values.
PRODUCT_DEVICE := togari
PRODUCT_MANUFACTURER := Sony
PRODUCT_MODEL := Xperia Z Ultra

# Inherit CM common Phone stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Enhanced NFC
$(call inherit-product, vendor/cm/config/nfc_enhanced.mk)

$(call inherit-product, device/sony/togari/aosp_c6803.mk)
$(call inherit-product, device/sony/togari/BoardConfig.mk)
$(call inherit-product, device/sony/togari/BoardConfigCM.mk)

#PRODUCT_BUILD_PROP_OVERRIDES += PRODUCT_NAME=C6833
#PRODUCT_BUILD_PROP_OVERRIDES += BUILD_FINGERPRINT=Sony/C6833/C6833:5.0.2/14.5.A.0.270/3305956307:user/release-keys
#PRODUCT_BUILD_PROP_OVERRIDES += PRIVATE_BUILD_DESC="C6833-user 5.0.2 14.5.A.0.270 3305956307 release-keys"

PRODUCT_NAME := cm_sony_aosp_togari
PRODUCT_DEVICE := togari
